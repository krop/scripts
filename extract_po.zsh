#
# GOAL: Designed for the openSUSE unstable repository (KDE:Unstable:Extra) to
# provide the latest translations for the packages from this repository.
# This is particularly useful for very active applications (e.g: kdevelop)
#
# NOTE: only tested with ZSH.
#
# Author: Christophe Giboudeaux
#
# if you already have the release-tools repository checked out, you can run
# export RELEASE_TOOLS_DIR=/path/to/release-tools before calling extract_po
# the macro will download the repository otherwise.
#
# Example:
#
# ``source /path/to/extract_po.zsh``
# ``extract_po kdevelop5 kdevelop``
#

RELEASE_TOOLS_DIR=/data/kde/src/release-tools

extract_po() {
  if [[ $# -ne 2 ]]; then
    print "Usage: extract_po <package name> <git repository name>"
    print "Where:"
    print "'package name' is the translations tarball name"
    print "'git repository name' is the real GIT folder name"
    print "(ie: browsable using https://websvn.kde.org/trunk/l10n-kf5/<some language>/messages/<repository>)"
    return 1
  fi

  # Parameters
  # 1: The package name.
  # 2: The GIT repository name.
  local packageName="$1"
  local srcdir="$2"

  autoload -U zargs
  unsetopt print_exit_value

  local _svnServer="svn://anonsvn.kde.org/home/kde"

  # Use /tmp if TMPDIR isn't defined
  [[ -z $TMPDIR ]] && TMPDIR="/tmp"

  local po="po"
  local podirPath="${TMPDIR}/po-${packageName}"
  local podir="${podirPath}/${po}"

  # If a translations directory exists, delete it before going further
  [[ -e ${podir} ]] && rm -fr ${podir}

  # Try to find translations for all languages unless a list is given
  local -a langs
  typeset -U langs

  # Released languages:
  if [[ -z $RELEASE_TOOLS_DIR ]]; then
    [[ ! -d ${TMPDIR}/release-tools ]] && $(git clone https://invent.kde.org/sysadmin/release-tools.git ${TMPDIR}/release-tools) || ( cd ${TMPDIR}/release-tools && git pull )
    RELEASE_TOOLS_DIR=${TMPDIR}/release-tools
  fi

  langs=($(<${RELEASE_TOOLS_DIR}/language_list))

  mkdir -p ${podir}
  pushd ${podir}

  foreach language (${langs}){
    echo "\033[1mExtracting ${language} translations... \033[0m"
    svn co -q --depth files ${_svnServer}/trunk/l10n-kf5/${language}/messages/${srcdir} ${language} &> /dev/null
  }
  # cleanup
  /bin/rm -fr */.svn
  /bin/rm -f */*._desktop_.po &> /dev/null
  /bin/rm -f */*._json_.po &> /dev/null
  /bin/rm -f */*.appdata.po &> /dev/null
  /bin/rm -f */*_xml_mimetypes.po &> /dev/null

  print "\033[1mExtraction done. Now creating the tarball...\033[0m"

  # Remove empty dirs FIXME

  # Used to report the final list of catalogs
  local -a catalogs
  typeset -U catalogs

  poFiles=(*/*.po)
  catalogs=(${${poFiles:t}:r})

  # Pack the translations
  pushd ${podirPath}
  tar -cJf ${packageName}-lang.tar.xz ${po}
  mv ${podirPath}/${packageName}-lang.tar.xz ${TMPDIR}/${packageName}-lang.tar.xz
  popd

  # Cleanup
  popd
  rm -fr ${podirPath}

  print "\033[1mDone ! The translations are packed in ${TMPDIR}/${packageName}-lang.tar.xz\033[0m"
  print "\033[1mThe following catalogs were extracted: ${catalogs}\033[0m"
}
